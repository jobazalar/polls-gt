package models;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;

/**
 *
 * @author Bazalar
 */
@Entity
@Table(name = "votantes")
public class Votante extends Model {
    
    @ManyToOne
    @JoinColumn(name = "idencuestas", nullable = false)
    public Encuesta encuesta;    
    @Column(name = "email", nullable = false)
    public String email;
    
    public Votante (Encuesta encuesta, String email) {
        this.encuesta = encuesta;
        this.email = email;
    }
    
    public static Votante buscarPorEmailEncuesta(String email, Encuesta encuesta) {
        return Votante.find("byEncuestaAndEmail", encuesta, email).first();
    }
    
    public void guardar() {
        this.save();
    }
}
