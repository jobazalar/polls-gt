package models;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;

/**
 *
 * @author Bazalar
 */
@Entity
@Table(name = "votos")
public class Voto extends Model {
    
    @ManyToOne
    @JoinColumn(name = "idencuestas", nullable = false)
    public Encuesta encuesta;
    @Column(name = "fecha", nullable = false)
    public Date fecha;
    
    public Voto(Encuesta encuesta, Date fecha) {
        this.encuesta = encuesta;
        this.fecha = fecha;        
    }
    
    public Voto guardar() {
        return this.save();
    }
}
