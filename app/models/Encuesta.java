package models;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;

/**
 *
 * @author Bazalar
 */
@Entity
@Table(name = "encuestas")
public class Encuesta extends Model {
    
    @Column(name = "nombre", nullable = false)
    public String nombre;
    @Lob
    @Column(name = "descripcion", nullable = false)
    public String descripcion;
    @Column(name = "total_candidatos", nullable = false)
    public int totalCandidatos;
    @Column(name = "total_votos", nullable = false)
    public int totalVotos;
    // Constantes
    public static final int TOTAL_VOTOS_DEFAULT = 0;
    public static final int VOTO = 1;
    
    public Encuesta(String nombre, String descripcion, int totalCandidatos) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.totalCandidatos = totalCandidatos;
        this.totalVotos = TOTAL_VOTOS_DEFAULT;
    }
    
    public static List<Encuesta> listar() {
        return Encuesta.findAll();
    }
    
    public static Encuesta encontrarPorId(long id) {
        return Encuesta.findById(id);
    }

    public void agregarVoto() {
        this.totalVotos += VOTO;        
    }
    
    public void guardar() {
        this.save();
    }
}
