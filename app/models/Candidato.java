package models;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;

/**
 *
 * @author Bazalar
 */
@Entity
@Table(name = "candidatos")
public class Candidato extends Model {
    
    @ManyToOne
    @JoinColumn(name = "idencuestas", nullable = false)
    public Encuesta encuesta;
    @Column(name = "nombre", nullable = false)
    public String nombre;
    @Column(name = "num_votos", nullable = false)
    public int numeroVotos;
    // Constantes
    public static final int NUM_VOTOS_INICIAL = 0;
    public static final float PORCENTAJE_VOTOS_INICIAL = 0;
    public static final float PORCENTAJE_MAXIMO = 100;
    public static final int VOTO = 1;
    
    public Candidato(Encuesta encuesta, String nombre) {
        this.encuesta = encuesta;
        this.nombre = nombre;
        this.numeroVotos = NUM_VOTOS_INICIAL;
    }
    
    public static Candidato encontrarPorId(long id) {
        return Candidato.findById(id);
    }
    
    public static List<Candidato> encontrarPorEncuesta(Encuesta encuesta) {
        return Candidato.find("byEncuesta", encuesta).fetch();
    }   

    public void agregarVoto() {
        this.numeroVotos += VOTO;
    }
    
    public double calcularPorcentaje() {       
        return (PORCENTAJE_MAXIMO * this.numeroVotos) / encuesta.totalVotos;
    }
    
    public void guardar() {
        this.save();
    }
}
