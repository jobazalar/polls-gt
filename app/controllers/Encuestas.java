package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import play.data.validation.Required;

public class Encuestas extends Controller {

    public static void listar() {
        List<Encuesta> encuestas = Encuesta.listar();
        render(encuestas);
    }

    public static void encuesta(long idEncuesta) {
        Encuesta encuesta = Encuesta.encontrarPorId(idEncuesta);
        notFoundIfNull(encuesta);
        List<Candidato> candidatos = Candidato.encontrarPorEncuesta(encuesta);        
        Collections.shuffle(candidatos);
        render(idEncuesta, encuesta, candidatos);
    }

    public static void votar(long idEncuesta, @Required String email) {     
        Encuesta encuesta = Encuesta.encontrarPorId(idEncuesta);
        notFoundIfNull(encuesta);

        String opcionMarcada = params.get("opciones");
        if (opcionMarcada == null) {
            flash.put("error-voto", "Debe seleccionar una opción.");
            encuesta(idEncuesta);
        }
        Long idCandidato = new Long(opcionMarcada);
        Candidato candidato = Candidato.encontrarPorId(idCandidato.longValue());
        notFoundIfNull(candidato);

        if (validation.hasErrors() || !validation.email(email).ok) {
            flash.put("error-email", "Debe ingresar un email válido, es obligatorio.");
            encuesta(idEncuesta);
        }

        Votante votante = Votante.buscarPorEmailEncuesta(email, encuesta);
        if (votante == null) {
            Voto voto = new Voto(encuesta, new Date());
            voto.guardar();
            votante = new Votante(encuesta, email);
            votante.guardar();
            encuesta.agregarVoto();
            encuesta.guardar();
            candidato.agregarVoto();
            candidato.guardar();

            flash.success("¡Gracias por votar!");
            resultado(idEncuesta);
        } else {
            flash.put("error-repetido", "Solo se permite un voto por email. Para completar debe ingresar otro email.");
            encuesta(idEncuesta);
        }
    }

    public static void resultado(long idEncuesta) {
        Encuesta encuesta = Encuesta.encontrarPorId(idEncuesta);
        notFoundIfNull(encuesta);
        List<Candidato> candidatos = Candidato.encontrarPorEncuesta(encuesta);
        HashMap porcentajes = new HashMap();
        for(Candidato candidato : candidatos) {
            double porcentaje = Math.round(candidato.calcularPorcentaje() * 10.0) / 10.0;
            porcentajes.put(candidato.id, porcentaje);
        }
        render(idEncuesta, encuesta, candidatos, porcentajes);
    }
}